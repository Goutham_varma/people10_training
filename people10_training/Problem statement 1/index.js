const express=require('express')
const app=express()
const path=require('path')
const pg=require('pg')
const string="postgres://postgres:people10dgv@localhost/products";
app.get('/api1', function (request, response, next) {
      pg.connect(string,function(err,client,done) {
      	if(err){
            console.log("No connection"+ err);
            response.status(400).send(err);
        }
        client.query('SELECT * FROM public.data',function(err,result) {
            done();
            if(err){
                console.log(err);
                response.status(400).send(err);
            }
            response.json(result.rows);
        });
      });
});
app.get('/api2', function (request, response, next) {
      pg.connect(string,function(err,client,done) {
        if(err){
            console.log("No connection"+ err);
            response.status(400).send(err);
        }
        client.query('SELECT itemcode,amount,amountafterdiscount FROM public.data',function(err,result) {
            done();
            if(err){
                console.log(err);
                response.status(400).send(err);
            }
            response.json(result.rows);
        });
      });
});
var dir=path.join(__dirname,'Application');
app.use(express.static(dir));
app.listen(3000,function(){
	console.log('Listening to port 3000')
});

