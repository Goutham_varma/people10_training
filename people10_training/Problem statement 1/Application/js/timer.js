var min = document.getElementById("minutes");
var sec = document.getElementById("seconds");
var tot= 0;
setInterval(setTime, 1000);
function setTime()
    {
        ++tot;
        sec.innerHTML = display(tot%60);
        min.innerHTML = display(parseInt(tot/60));
    }
function stop()
    {
        tot=0;  
    }
function display(val)
   {
        var string = val + "";
            if(string.length < 2)
            {
                return "0" + string;
            }
            else return string;
    }