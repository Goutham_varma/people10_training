(function(){var app = angular.module('myApp', ['ngRoute']);
app.config(['$routeProvider',function($routeProvider) {
  $routeProvider

  .when('/', {
    templateUrl : 'home.html',
    controller  : 'HomeController'
  })

  .when('/PS1', {
    templateUrl : 'PS1.html',
    controller  : 'PS1Controller'
  })

  .when('/PS2', {
    templateUrl : 'PS2.html',
    controller  : 'PS2Controller'
  })
  .when('/PS3', {
    templateUrl : 'PS3.html',
    controller  : 'PS3Controller'
  })
  .when('/PS4', {
    templateUrl : 'PS4.html',
    controller  : 'PS4Controller'
  })
  .when('/PS5', {
    templateUrl : 'PS5.html',
    controller  : 'PS5Controller'
  })
  .when('/PS6', {
    templateUrl : 'PS6.html',
    controller  : 'PS6Controller'
  })
  .when('/PS7', {
    templateUrl : 'PS7.html',
    controller  : 'PS7Controller'
  })
  .when('/PS8', {
    templateUrl : 'PS8.html',
    controller  : 'PS8Controller'
  })
  .when('/PS9', {
    templateUrl : 'PS9.html',
    controller  : 'PS9Controller'
  })
  .when('/success',{ 
    templateUrl : 'success.html',
    controller  : 'successcontroller'

  })
  .otherwise({redirectTo: '/'});
}]);

app.controller('HomeController', function($scope) {
});
app.controller('PS1Controller', function($scope,$location) {
  $scope.interests=[
      {name:"C++",ischecked:false},
      {name:"Java",ischecked:false},
      {name:"JavaScript",ischecked:false},
      {name:"HTML5",ischecked:false},
      {name:"CSS3",ischecked:false},
      {name:"Ruby",ischecked:false},
      {name:"SQL",ischecked:false},
      {name:"PHP",ischecked:false},
      {name:".NET",ischecked:false}
  ];
  $scope.check=0;
  $scope.limit=4;
  $scope.array=[];
  $scope.user={};
  $scope.checked=function(int){
    if(int.ischecked) $scope.check++;
    else $scope.check--;
    if($scope.check==4)
    {
      angular.forEach($scope.interests,function(int){
           if(int.ischecked) $scope.array.push(int.name);
      })
      $scope.user.interest=$scope.array.toString();
    }
    else {$scope.array=[];$scope.user.interest=$scope.array.toString();}  
  }
  $scope.nav=function(){
   $location.path('/success');
  }
  $scope.submit=function(){
    savedData.push($scope.user);
    $scope.user={};
  }
});
app.controller('PS2Controller', function($scope) {
  $scope.users=[];
    $scope.users=savedData;
});
app.controller('PS3Controller', function($scope) {
});
app.controller('PS4Controller', function($scope) {
});
app.controller('PS5Controller', function($scope) {
});
app.controller('PS6Controller', function($scope) {
});
app.controller('PS7Controller',['$http', '$scope',function($http,$scope) {
  $http.get('/api1').success(function(data){
      $scope.api1 = data
    });
  $http.get('/api2').success(function(data){
  $scope.api2 = data
  });
}]);
app.controller('PS8Controller', function($scope) {
});
app.controller('PS9Controller', function($scope) {
});
app.controller('successcontroller',['$scope','$location','$timeout',function($scope,$location,$timeout){
        $timeout(function() {
         $location.path('/PS2')
      }, 3000);
}]);
var savedData=[];
})();