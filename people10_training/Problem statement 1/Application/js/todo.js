var $task = $("#new-task");
var $todoTasks = $("#todo-tasks");
var $doneTasks = $("#done-tasks");
var $startedTasks = $("#started-tasks");
var $add = $("#add-button");
var makeNewItem = function(taskToAdd)
  {
     var Item = $("<li></li>");
     var Label = $("<label></label>");
     var Input = $("<input type='text' class='edit-text'>");
     var Edit = $("<button class='edit-button'>Edit</button>");
     var start = $("<button class='start-button'>Start</button>");
     var done = $("<button class='done-button'>Done</button>");
     var Delete = $("<button class='delete-button'>Delete</button>");
     Item.append(Label.html(taskToAdd))
         .append(Input)
         .append(Edit)
         .append(start)
         .append(done)
         .append(Delete);
     return Item;
  }
$add.on("click", function()
  {
    $todoTasks.append(makeNewItem($task.val()));
    $task.val("");
  }
)
var editTask = function (a, b, c)
  {
    a.removeClass("editMode");
    c.text(b.val());
    b.val("");
  }
$todoTasks.on( "click", ".delete-button", function()
  {
    $(this).parent().remove();  
  }
)
$todoTasks.on("click",".start-button",function()
  {
    var Item=$(this).parent();
    $startedTasks.append(Item);
  }
)
$todoTasks.on( "click", ".edit-button", function()
  {
    var $a = $(this).parent();  
    var $b= $(this).prev();
    var $c = $b.prev();
    editTask($a, $b, $c);
  }
)
$startedTasks.on("click",".done-button",function()
  {
    var Item = $(this).parent();
    $doneTasks.append(Item);
  }
)
$startedTasks.on("click",".delete-button",function()
  {
    $(this).parent().remove();
  }
)
$startedTasks.on("click",".edit-button",function()
  {
    var $a = $(this).parent();  
    var $b = $(this).prev();
    var $c = $b.prev();
    editTask($a, $b, $c);
  }
)
$doneTasks.on( "click", ".delete-button", function()
  {
    $(this).parent().remove();  
  }
)
$doneTasks.on( "click", ".start-button", function()
  {
    var Item = $(this).parent();
    $startedTasks.append(Item);
  }
)





